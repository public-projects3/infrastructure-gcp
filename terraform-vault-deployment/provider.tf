terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.90.1"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "1.13.4"
    }
    helm = {
      source = "hashicorp/helm"
      version = "1.3.2"
    }
  }
}

provider "google" {
}

provider "kubernetes" {
  config_path = "config"
  host  = google_container_cluster.kubernetes_cluster.endpoint
  token = data.google_client_config.default.access_token

  cluster_ca_certificate = base64decode(
    google_container_cluster.kubernetes_cluster.master_auth.0.cluster_ca_certificate
  )
}

provider "helm" {
  kubernetes {
    config_path = "config"
    host  = google_container_cluster.kubernetes_cluster.endpoint
    token = data.google_client_config.default.access_token

    cluster_ca_certificate = base64decode(
      google_container_cluster.kubernetes_cluster.master_auth.0.cluster_ca_certificate
    )
  }
}